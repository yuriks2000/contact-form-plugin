# README #

### Wordpress plugin to send contacts to Hubspot ###

#### Steps to install ####

1. Download zip to your computer
2. Go to wordpress "Plugins" -> "Add new" -> "Upload Plugin"
3. Add zipped file and press "Install now"
4. Activate your plugin

### How to use? ###

* Just include shortcode in the text editor "[email_form]" anywhere in your theme
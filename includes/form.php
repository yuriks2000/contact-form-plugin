<?
// function that runs when shortcode is called

	
function form_html() { 

$message = '
<div class="container">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal email-form" action="" method="post">
          <fieldset>
            <legend class="text-center">'.__("Contact us", "contact-form-plugin-yuriy").'</legend>
    
            <!-- First Name input-->
            <div class="form-group">
              <label class="col-md-12 control-label" for="first_name">'.__("First Name", "contact-form-plugin-yuriy").'*</label>
              <div class="col-md-9">
                <input id="first_name" name="first_name" type="text" placeholder="'.__("John", "contact-form-plugin-yuriy").'" class="form-control">
              </div>
            </div>
            
            <!-- Last Name input-->
            <div class="form-group">
              <label class="col-md-12 control-label" for="last_name">'.__("Last Name", "contact-form-plugin-yuriy").'</label>
              <div class="col-md-9">
                <input id="last_name" name="last_name" type="text" placeholder="'.__("Smith", "contact-form-plugin-yuriy").'" class="form-control">
              </div>
            </div>
            
            <!-- Subject input-->
            <div class="form-group">
              <label class="col-md-12 control-label" for="subject">'.__("Subject", "contact-form-plugin-yuriy").'</label>
              <div class="col-md-9">
                <input id="last_name" name="subject" type="text" placeholder="'.__("Inquiry", "contact-form-plugin-yuriy").'" class="form-control">
              </div>
            </div>
            
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-12 control-label" for="message">'.__("Your message", "contact-form-plugin-yuriy").'</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="'.__("Please enter your message here...", "contact-form-plugin-yuriy").'" rows="5"></textarea>
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-12 control-label" for="email">'.__("E-mail", "contact-form-plugin-yuriy").'*</label>
              <div class="col-md-9">
                <input id="email" name="email" type="text" placeholder="'.__("email@email.com", "contact-form-plugin-yuriy").'" class="form-control">
              </div>
            </div>    
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <div class="message-error"></div>
                <div class="message-error-dev"></div>
                <div class="message-success"></div>
                <button type="submit" class="btn btn-primary btn-lg">'.__("Submit", "contact-form-plugin-yuriy").'</button>                
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>'; 
 
// Output needs to be return
return $message;
} 
// register shortcode
add_shortcode('email_form', 'form_html'); 

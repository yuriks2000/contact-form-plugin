<?
add_action("wp_ajax_MyPlugin_GetVars", "MyPlugin_GetVars");
add_action("wp_ajax_nopriv_MyPlugin_GetVars", "MyPlugin_GetVars");

function MyPlugin_GetVars(){
	
	/**
	 * Variables
	 */
	$response = array(); // stores response to our contact form
    $formData = array(); // stores our user data
	$options = get_option('plugin_options');	


	/**
	 * Functions
	 */
	 	 	
	// function to log our contacts
		
	function logTofile($data){
		$fp = fopen( dirname(__FILE__).'/log.txt', 'a');//opens file in append mode.
				
		if (is_array($data)){
			fwrite($fp, "\n\n" . date('Y-m-d h:i:sa') . " - NEW contact");
			foreach ($data as $key => $text){
				fwrite($fp, "\n" . $key . " - " . $text);			
			}				
		}else {
			fwrite($fp, "\n\n" . date('Y-m-d h:i:sa'));
			fwrite($fp, "\n" . json_encode($data));
		}
		fclose($fp);
	}

	// function to send email
	
	function sendEmail($data){		
		$message = '';
		$options = get_option('plugin_options');
		foreach ($data as $key => $text){
			$message .= "\n" . $key . ": " . $text;			
		}					
		$to      = $options["email_to"];
		$subject = $data["subject"];
		return wp_mail( $to, $subject, $message);	
	}
	
	/**
	 * Validation
	 */	
	
	// if this is a post request
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	  $formData = $_POST["formData"];	  	  
	  // required filds with validation
	  
	   // validate email - cannot be empty
	  if ($formData["email"] == ""){
		$response['error'][] =  __("Email cannot be empty", "contact-form-plugin-yuriy");
	  	$response['invalid'][] = 'email';
	  }	
	  // validate email - proper format
	  else if (!filter_var($formData["email"], FILTER_VALIDATE_EMAIL)) {	  
	  	$response['error'][] = "'" . $formData["email"] . "' " .   __("is not a valid email", "contact-form-plugin-yuriy");
	  	$response['invalid'][] = 'email';
	  }
	  
	  // validate first name - cannot be empty
	  if ($formData["first_name"] == ''){
		 $response['error'][] = __("First name cannot be empty", "contact-form-plugin-yuriy"); 
		 $response['invalid'][] = 'first_name';
	  }
	}
	
	/**
	 * Send to Hubspot
	 */	
	
	
	// if there are no validation errors lets send the contact to Hubspot !
	if ( !isset($response['error']) && $options["hotspot_api"] != ''){
	
		$arr = array(
            'properties' => array(
                array(
                    'property' => 'email',
                    'value' => $formData["email"]
                ),
                array(
                    'property' => 'firstname',
                    'value' => $formData["first_name"]
                ),
                array(
                    'property' => 'lastname',
                    'value' => $formData["last_name"]
                ),                
                array(
                    'property' => 'message',
                    'value' => $formData["message"]
                )
            )
        );        
        $post_json = json_encode($arr);
        $hapikey = $options["hotspot_api"];
        $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_hub = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        
        $response['curl_errors'] = json_decode($curl_errors);
        $response['status_code'] = json_decode($status_code);
        $response['hubspot_response'] = json_decode($response_hub);
        
        // 
	    logTofile($formData);	    
        // if there are no erros lets send a success message to frontend, log our data, send email
        if ($response['curl_errors'] == "" && $response['status_code'] == "200"){
	    	
	    	$response['success'] = true; 
	    	// if our "email to" are not empty in settings lets email our contacts 
	    	
	    	if ($options["email_to"] != ''){
		    	$email_result = sendEmail($formData);
		    	if ($email_result == true){
			    	logTofile("Email sent successfully"); 	
			    	$response['email_result'] = "Email sent successfully";				    
			    }else {
			    	logTofile("Email not sent "); 
			    	$response['email_result'] = $email_result;				    
			    }
	    	}	    	
        }else {
	        logTofile(
	        	"Hotspot error response: " . $response_hub . "\n" .
	        	"Status code: " . $status_code . "\n" .
	        	"Curl errors: " . $curl_errors
	        	); 		
	        $response['error'][] = $response['hubspot_response']->message;
        }
        
	}
	echo json_encode($response); 
	
	die();   
}
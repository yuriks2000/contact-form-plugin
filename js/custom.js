jQuery(document).ready(function($){
	
	var messageSuccess = $('.email-form .message-success'); 	  
	var messageError = $('.email-form .message-error');
	  
	$('.email-form').submit(function (e) {
	  e.preventDefault();
	  const $form = $(this);	   	  
	  
	  function getFormData($form){
		    let unindexed_array = $form.serializeArray();
		    let indexed_array = {};
		
		    $.map(unindexed_array, function(n, i){
		        indexed_array[n['name']] = n['value'];
		    });
		
		    return indexed_array;
		}
		var data = getFormData($form);
	  
	  $.ajax({
	    type: 'POST',
	    url: myAjax.ajaxurl,
	    dataType : "json",
	    data: {action : 'MyPlugin_GetVars', formData : data},	    
	    success: function (response) {
// 		    console.log(response);		    
		    messageError.html("");
		    messageSuccess.html("");	
		    // if there are errors append them
		    if (response.error != undefined){
			    $.each(response.error, function(index, element) {
					messageError.append('<div>'+element+'</div>');
		        });
		        $.each(response.invalid, function(index, element) {
		            $('.email-form #'+ element).addClass('invalid');
		        });		    			    
		    }
		    // success echo Message succesfuly sent! and clear form
		    else if (response.success != undefined){			    
	            messageSuccess.append('<div>Message succesfuly sent!</div>');		        
	            $('.email-form input, .email-form textarea').val("");
		    }		    
	      
	    },
	    error: function(error){
// 		    console.log(error);
		    $('.email-form .message-error-dev').append(error);
	    }
	  });
	});
	
});
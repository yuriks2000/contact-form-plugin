<?php
/**
 * Plugin Name: Email form plugin with Hotspot integrations
 * Plugin URI: http://test.24deliveryapp.ru
 * Description: Creates a shortcode [email_form] to use in your theme implementing a contact form to send contacts to Hubspot and email results
 * Version: 1.0.1
 * Author: Yuriy
 * Author URI: http://test.24deliveryapp.ru
 */
 
 
 /**
 * Initialise settings page with options
 */

add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page() {
	add_options_page('Email Hotspot Plugin Page', 'Hotspot Options', 'manage_options', 'plugin-form-yuriy', 'plugin_options_page');
}

function plugin_options_page() {
?>
<div>
	<h2>Email Hotspot Plugin Page</h2>	
	<form action="options.php" method="post">
		<?php settings_fields('plugin_options'); ?>
		<?php do_settings_sections('plugin-form-yuriy'); ?>	
		<input name="Submit" type="submit" value="<?php echo __("Save changes", "contact-form-plugin-yuriy"); ?>" />
	</form>
</div>
 
<?php
}

add_action('admin_init', 'plugin_admin_init');
function plugin_admin_init(){
	register_setting( 'plugin_options', 'plugin_options' );
	add_settings_section('plugin_main', __("Main Settings", "contact-form-plugin-yuriy"), 'plugin_section_text', 'plugin-form-yuriy');
	add_settings_field('plugin_hotspot_api', __("Hotspot api key", "contact-form-plugin-yuriy"), 'plugin_setting_hotspot_api', 'plugin-form-yuriy', 'plugin_main');
	add_settings_field('plugin_email_to', __("Email to", "contact-form-plugin-yuriy"), 'plugin_setting_email_to', 'plugin-form-yuriy', 'plugin_main');
}

function plugin_section_text() {
	echo '<h4>Include shortcode [email_form] to add your form to any page.</h4>';
}

function plugin_setting_hotspot_api() {
	$options = get_option('plugin_options');
	echo "<input id='plugin_hotspot_api' name='plugin_options[hotspot_api]' size='40' type='text' value='{$options['hotspot_api']}' />";
	
}
function plugin_setting_email_to() {
	$options = get_option('plugin_options');
	echo "<input id='plugin_email_to' name='plugin_options[email_to]' size='40' type='text' value='{$options['email_to']}' />";
}


/**
 * include file with shortcode that stores our bootstap html form 
 */

include( plugin_dir_path( __FILE__ ) . 'includes/form.php' );


/**
 * Enque our js and css
 */
 
function my_load_scripts($hook) {
 
    // create my own version codes
    $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'js/custom.js' ));
    $my_css_ver = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'style.css' ));
     
    // 
    wp_register_script( "custom_js",  plugins_url( 'js/custom.js', __FILE__ ), array('jquery'), $my_js_ver );
	wp_localize_script( 'custom_js', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
    wp_enqueue_script( 'custom_js');
    
    wp_enqueue_style( 'my_css',    plugins_url( 'style.css',    __FILE__ ), false,   $my_css_ver );
     
}
add_action('wp_enqueue_scripts', 'my_load_scripts');


/**
 * include our form validation and sending to Hotspot
 */

include( plugin_dir_path( __FILE__ ) . '/includes/hubspot.php' );

/**
 * Settings link to plugin
 */

function my_plugin_settings_link($links) { 
  $settings_link = '<a href="options-general.php?page=plugin-form-yuriy.php">'.__("Settings", "contact-form-plugin-yuriy").'</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'my_plugin_settings_link' );

?>